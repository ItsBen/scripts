#!/bin/bash

  source build/envsetup.sh
export ALLOW_MISSING_DEPENDENCIES=true
export LC_ALL="C"
export USE_CCACHE="1"
export OF_MAINTAINER="ItsBen12000"
export FOX_VERSION="R11.0"
export FOX_R11="1"
export FOX_ADVANCED_SECURITY="1"
export FOX_RESET_SETTINGS="1"
export TARGET_ARCH=ARM64
export DEVICE_RESOLUTION=2340x1080
export OF_SCREEN_H=2340
export FOX_USE_NANO_EDITOR=1
export OF_DONT_PATCH_ENCRYPTED_DEVICE=1
export OF_DONT_PATCH_ON_FRESH_INSTALLATION=1
export OF_FLASHLIGHT_ENABLE=1
export OF_OTA_RES_DECRYPT=1
export OF_STATUS_INDENT_LEFT=48
export OF_STATUS_INDENT_RIGHT=48
export OF_USE_MAGISKBOOT=1
export OF_USE_MAGISKBOOT_FOR_ALL_PATCHES=1
export OF_USE_LOCKSCREEN_BUTTON=1
export OF_QUICK_BACKUP_LIST="/boot;/data;/system_image;/vendor_image;"
export OF_DISABLE_MIUI_OTA_BY_DEFAULT=1
export FOX_DELETE_AROMAFM=1
export FOX_BUILD_TYPE="Beta"
lunch omni_tucana-eng && mka recoveryimage
